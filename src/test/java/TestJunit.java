import io.qameta.allure.Allure;
import io.qameta.allure.Issue;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import automationTesting.ModuleTester;
import automationTesting.allureReport.Attachments;
import automationTesting.allureReport.JUnit5LoggingExtension;
import automationTesting.allureReport.parameters.EnvironmentParameters;
import automationTesting.allureReport.parameters.StepParameters;
import automationTesting.allureReport.parameters.TestParameters;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static automationTesting.allureReport.JUnit5LoggingExtension.setStatus;
import static automationTesting.allureReport.JUnit5LoggingExtension.statuses;
import static automationTesting.allureReport.Steps.result;


/**
 * Путь к аттачментам:
 * build\reports\path\to\project\module\build\allure-results
 */
@ExtendWith({JUnit5LoggingExtension.class})
@TestInstance(PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestJunit {
    private static final Logger log = LoggerFactory.getLogger(TestJunit.class);


    @BeforeAll
    static void printBeforeAllLog() {
        System.setProperty("ModuleTester.logPath", System.getProperty("user.dir") + "\\build\\logs\\log.log");
        EnvironmentParameters
                .add("Версия приложения", "1.765.345")
                .add("Версия БД", "1.563");
        log.error("BeforeALL");
    }

    @AfterAll
    static void after() {
        // для проверки, что параметры берутся из любого места
        EnvironmentParameters
                .add("Дамп", "777");
    }

    @BeforeEach
    void printBeforeEachLog() {
        log.error("BeforeEACH");
    }

    @Test
    @Order(1)
    void emptyTest1() {
    }

    @Test
    @Tag("дефект_1_582.761")
    @Order(2)
    void singleFailedTest2() {
        result("Тэг добавлен");
        fail("test2 failed");
    }

    @Test
    @Order(3)
    void afterAllTest() {
        log.info("System.getProperty(\"AllureExtension.toAttachEntireLog\")='{}'", System.getProperty("AllureExtension.toAttachEntireLog"));
        System.setProperty("AllureExtension.toAttachEntireLog", "true");
        log.info("System.getProperty(\"AllureExtension.toAttachEntireLog\")='{}'", System.getProperty("AllureExtension.toAttachEntireLog"));
    }

    @ParameterizedTest
    @MethodSource("dataProvider")
    @Order(4)
    @Issue("254")
    void multiplyPassedTest3(Data d) {
        passedTest(d.val1, d.val2, d.val3);
//        passedTest(d.val1, d.val2, d.val3);
    }

    @ParameterizedTest
    @MethodSource("dataProvider")
    @Order(5)
    void multiplyFailedTest4(Data d) {
//        failedTest(d.val1, d.val2, d.val3);
    }

    String _currentTestUUID = Allure.getLifecycle().getCurrentTestCase().orElse("");

    @Test
    @Order(6)
    void beforeTestExecution() {
        String currentTestUUID = Allure.getLifecycle().getCurrentTestCase().orElse("");
        Assertions.assertNotEquals(_currentTestUUID, currentTestUUID);
        Assertions.assertFalse(TestParameters.nonEmpty());
        Assertions.assertTrue(statuses.isEmpty());
    }

    @Test
    @Order(7)
    void test_setStatus() {
        passedTest("a3dfs33a", 2, true);
        Assertions.assertTrue(statuses.containsValue(Status.SKIPPED));
    }

    @Step
    void passedTest(String val1, Integer val2, boolean val3) {
        //region Проверять вручную в сформированном отчете
        TestParameters
                .add("value1", val1)
                .add("value2", val2)
                .add("value3", val3);

        StepParameters
                .add("val1_param", val1)
                .add("val2_param", val2)
                .add("val3_param", val3);
        //endregion

        attachTable();
        if (val1.equals("a3dfs33a"))
            setStatus(Status.SKIPPED);
//        Attachments.attachFile("C:\\Users\\Aleksei\\IdeaProjects\\allureextension_office\\src\\test\\resources\\table.html", true);

        log.info(val1);
        log.info(String.valueOf(val2));
        log.info(String.valueOf(val3));
    }

    //    @Step
    @Test
    void handleTestExecutionException_methodTest() {
        //region имитируем случай с NPE
        String nullString = null;
        try {
            nullString.equals("");
        } catch (NullPointerException e) {
            Assertions.assertThrows(NullPointerException.class,
                    () -> new JUnit5LoggingExtension().handleTestExecutionException(null, e));
        }//endregion

        // проверяем наличие сообщения об ошибке в файле лога
        Assertions.assertTrue(ModuleTester
                .logContainsRows("java.lang.NullPointerException",
                        "\tat TestJunit.failedTest(TestJunit.java:"));
    }

    static Stream<Data> dataProvider() {
        return Stream.of(
                new Data("adfsa", 1, true),
                new Data("a2df22sa", 222222222, true),
                new Data("a3dfs33a", 3, false),
                new Data("ad44f4sa", 4, true),
                new Data("ad-5fs-5a", -5, false)
        );
    }


    public static void attachTable() {
        String table = "<!doctype html>\n" +
                "<html lang=\"ru\">\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\" />\n" +
                "  <title></title>\n" +
                "  <link rel=\"stylesheet\" href=\"style.css\" />\n" +
                "</head>\n" +
                table() +
                "</body>\n" +
                "</html>";
        Attachments.attachFile("table", "html", table.getBytes());
    }

    private static String table() {
        String tableFromFile = "";
        try {
            tableFromFile = new String(
                    Files.readAllBytes(
                            Paths.get(ClassLoader.getSystemResource("таблица.txt").toURI()))
            );
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return tableFromFile;
    }
}

class Data {
    String val1;
    Integer val2;
    boolean val3;

    public Data(String val1, Integer val2, boolean val3) {
        this.val1 = val1;
        this.val2 = val2;
        this.val3 = val3;
    }

    @Override
    public String toString() {
        return "Data{" +
                "val1='" + val1 + '\'' +
                ", val2=" + val2 +
                ", val3=" + val3 +
                '}';
    }
}