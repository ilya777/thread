import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import automationTesting.allureReport.JUnit5LoggingExtension;
import automationTesting.allureReport.annotations.Log;
import automationTesting.allureReport.annotations.Logs;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static automationTesting.allureReport.enums.PathTypes.ABS;
import static automationTesting.allureReport.enums.PathTypes.USER_DIR_BASED;

@ExtendWith({JUnit5LoggingExtension.class})
@TestInstance(PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Log(path = "logs\\log.log", type = USER_DIR_BASED)
public class TestsAttachLogFiles {

//TODO: дописать проверки

    @Test
    @Logs({@Log(path = "C:\\Users\\Daniil\\IdeaProjects\\allureextension_office\\build\\logs\\log2.log", type = ABS),
           @Log(path = "logs\\log.log", type = USER_DIR_BASED)})
    @Order(1)
    void test1() {
        System.out.println("test1");
    }

    @Test
    @Order(2)
    void test2() {
        System.out.println("test2");
    }

    @Test
    @Log(path = "logs\\log2.log", type = USER_DIR_BASED)
    @Order(3)
    void test3() {
        System.out.println("test3");
    }

//    @Test
//    @Log(path = "logs\\log2.log", buildBased = true, onlyOnFail = true)
//    @Order(4)
//    void test4() {
//        System.out.println("test3");
//        fail("fail test4");
//    }

}
