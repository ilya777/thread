package automationTesting.allureReport;

import automationTesting.allureReport.model.AllureStep;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static automationTesting.allureReport.Steps.result;
import static automationTesting.allureReport.Steps.step;
import static java.util.stream.Collectors.toCollection;

class StepsTests {

    /**
     * Единый тест для проверки:
     *
     * @see Steps#history() ()
     * @see Steps#currentStep()
     * @see Steps#currentSteps()
     * @see AllureStep#depth
     * @see AllureStep#linkToCall()
     * @see AllureStep#number()
     * @see AllureStep#toString()
     * @see AllureStep#toShortString()
     */
    @Test
    void allInOne() {
        step("[Step 1]: T111 - Название ТК 1", () -> {
        });

        step("[Steps 2 - 4]: T222 - Название ТК 2", () -> {
            Assertions.assertAll(
                    () -> assertStepsStackSize(1),
                    () -> assertHistory(1, "[Step 1]: T111 - Название ТК 1"),
                    () -> assertAllInCurrentStep(
                            0,
                            "(StepsTests.java:31)",
                            "#2-4",
                            "StepsTests::allInOne(): #2-4",
                            "->StepsTests::allInOne(): [Steps 2 - 4]: T222 - Название...<-")
            );

            Steps.step("step...");

            step("[Step 1]: короткое имя", () -> {
                final int expStackSize = 2;
                final int expDepth = 1;
                final String expLinkToCall = "(StepsTests.java:45)";
                final String expNumber = "#1";
                final String expToShortString = "#1";
                final String expToString = "StepsTests::allInOne(): #2-4 { ->[Step 1]: короткое имя<- }";
                Assertions.assertAll(
                        () -> assertStepsStackSize(expStackSize),
                        () -> assertHistory(1, "[Step 1]: T111 - Название ТК 1"),
                        () -> assertAllInCurrentStep(expDepth, expLinkToCall, expNumber, expToShortString, expToString)
                );
//                $log().info(Steps.currentStep().toString());

                // выполнение вложенного шага
                new SimpleTest1().t2(1);

                // проверка, что после выполнения вложенного шага изменилась только история
                Assertions.assertAll(
                        () -> assertStepsStackSize(expStackSize),
                        () -> assertHistory(
                                3, "[Steps 1 - 11]: T2 - Простой тест для проверки allInOne"),
                        () -> assertAllInCurrentStep(expDepth, expLinkToCall, expNumber, expToShortString, expToString)
                );
            });

            step("[Steps 2 - 5]: T333 - Название ТК 3", this::otherMethod);

        });
    }

    //=================================================
    //============= Кастомные assertion'ы =============
    //region //=================================================

    static void assertStepsStackSize(int expSize) {
        Assertions.assertEquals(expSize, Steps.currentSteps().count(), "Неправильный размер стэка");
    }

    static void assertHistory(int expSize, String lastAddedStepName) {
        Assertions.assertAll(
                () -> Assertions.assertEquals(expSize, Steps.history().count(), "Неправильный размер истории"),
                () -> Assertions.assertEquals(
                        lastAddedStepName,
                        Steps.history().collect(toCollection(LinkedList::new)).getLast().name,
                        "Последний добавленный в историю шаг"
                )
        );
    }

    static void assertAllInCurrentStep(int expDepth, String linkToCall, String number, String toShortString, String toString) {
        Assertions.assertAll(
                () -> Assertions.assertEquals(expDepth, Steps.currentStep().depth, "Кол-во родителей"),
                () -> Assertions.assertEquals(linkToCall, Steps.currentStep().linkToCall(), "Ссылка на строку"),
                () -> Assertions.assertEquals(number, Steps.currentStep().number(), "Порядковый номер"),
                () -> Assertions.assertEquals(toShortString, Steps.currentStep().toShortString(), "Результат toShortString()"),
                () -> Assertions.assertEquals(toString, Steps.currentStep().toString(), "Результат toString()")
        );
    }
    //endregion

    void otherMethod() {
        step("Без номера; в другом методе 1", () -> {
        });

        step("Без номера; в другом методе 2", () -> {
            final int expHistorySize = 5;
            final String expLastInHistoryName = "Без номера; в другом методе 1";
            Assertions.assertAll(
                    () -> assertStepsStackSize(3),
                    () -> assertHistory(expHistorySize, expLastInHistoryName),
                    () -> assertAllInCurrentStep(
                            2,
                            "(StepsTests.java:109)",
                            "№2",
                            "StepsTests::otherMethod(): №2",
                            "StepsTests::allInOne(): #2-4 { #2-5 { " +
                                    "->StepsTests::otherMethod(): Без номера; в другом методе 2<- } }")
            );

            step("[Steps 1-10]:  действие 333.1", () -> {
                // проверка, что не изменилась только история
                Assertions.assertAll(
                        () -> assertStepsStackSize(4),
                        () -> assertHistory(expHistorySize, expLastInHistoryName),
                        () -> assertAllInCurrentStep(
                                3,
                                "(StepsTests.java:124)",
                                "#1-10",
                                "#1-10",
                                "StepsTests::allInOne(): #2-4 { #2-5 { StepsTests::otherMethod(): №2 { " +
                                        "->[Steps 1-10]:  действие 333.1<- } } }")
                );

                new SimpleTest1().t2(2);
            });
        });
    }
}

class SimpleTest1 {

    /**
     * @param caseNum Номер случая.
     *                Определяет стек и историю,
     *                а соответственно, и глубину
     *                вложенности и список родителей
     */
    void t2(int caseNum) {
        step("[Steps 1 - 11]: T2 - Простой тест для проверки allInOne", () -> {
            final String expLinkToCall = "(StepsTests.java:153)";
            final String expNumber = "#1-11";
            final String expToShortString = "SimpleTest1::t2(): #1-11";

            switch (caseNum) {
                case 1:
                    Assertions.assertAll(
                            () -> StepsTests.assertStepsStackSize(3),
                            () -> StepsTests.assertHistory(1, "[Step 1]: T111 - Название ТК 1"),
                            () -> StepsTests.assertAllInCurrentStep(2, expLinkToCall, expNumber, expToShortString,
                                    "StepsTests::allInOne(): #2-4 { #1 { " +
                                            "->SimpleTest1::t2(): [Steps 1 - 11]: T2 - Простой т...<- } }")
                    );
                    break;
                case 2:
                    Assertions.assertAll(
                            () -> StepsTests.assertStepsStackSize(5),
                            () -> StepsTests.assertHistory(5, "Без номера; в другом методе 1"),
                            () -> StepsTests.assertAllInCurrentStep(4, expLinkToCall, expNumber, expToShortString,
                                    "StepsTests::allInOne(): #2-4 { #2-5 { StepsTests::otherMethod(): №2 { #1-10 { " +
                                            "->SimpleTest1::t2(): [Steps 1 - 11]: T2 - Простой т...<- } } } }")
                    );
                    break;
                default:
                    Assertions.fail("непроверяемый случай");
            }

            result("Успех");
        });
    }
}