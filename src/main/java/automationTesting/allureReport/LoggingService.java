package automationTesting.allureReport;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import automationTesting.allureReport.annotations.Log;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.lang.String.join;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.util.Arrays.asList;
import static automationTesting.allureReport.Attachments.attachFile;
import static automationTesting.allureReport.CommonUtils.logsFromAnnotations;


public class LoggingService {
    static final Logger _log = LoggerFactory.getLogger(LoggingService.class);

    /**
     * Аналог объявлению локального логгера
     *
     * Преимущества:
     * + не нужно объявлять локальный логгер в каждом классе
     * + автоопределение класса-источника
     */
    public static Logger $log() {
        return LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
    }

    /**
     * Хранит логи для текущего теста
     */
    private static final List<Logg> logs = new ArrayList<>();

    static void beforeTest(ExtensionContext context) {
        logs.clear();
        logs.addAll(logsFromAnnotations(context));
    }

    public static void afterTest(@SuppressWarnings("unused") ExtensionContext context) {
        logs.forEach(l -> {
            _log.info("\t+ log file: '{}'", l.path);
            l._attachLog(l);
        });
    }

    public static void handleException(@SuppressWarnings("unused") ExtensionContext context, Throwable throwable) {
        logs.forEach(l -> {
            l.isFailedTest = true;
            if (l.appendStacktrace)
                l._logAppend(Logg._stackTrace(throwable));
        });
    }
}

class Logg {
    String path;
    int firstLineOfTest;
    boolean onlyOnFail;
    boolean isFailedTest;
    boolean appendStacktrace;

    Logg(Log log) {
        switch (log.type()) {
            case ABS: path = log.path();
                break;
            case USER_DIR_BASED: path = Paths.get(System.getProperty("user.dir"), log.path()).toString();
                break;
            case USER_HOME_BASED: path = Paths.get(System.getProperty("user.home"), log.path()).toString();
        }
        firstLineOfTest = _lastLine();
        onlyOnFail = log.onlyOnFail();
        appendStacktrace = log.appendStacktrace();
    }

    /**
     * Возвращает индекс последней строки в общем логе
     */
    private int _lastLine() {
        File file = new File(path);
        try {
            return file.exists() ? (int) Files.lines(file.toPath()).count() : 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return firstLineOfTest;
    }

    /**
     * Прикрепляет лог к текущему тесту
     */
    void _attachLog(Logg log) {
        if (log.isFailedTest || !log.onlyOnFail) {
            LoggingService._log.warn("Прикладывается файл лога {} (lines {} - {} in log.log)", path, firstLineOfTest, _lastLine() + 1);
            attachFile(path, ".log", join("\n", _logLines(firstLineOfTest, _lastLine())).getBytes());
        }
    }

    /**
     * Список строк лога от firstLine до lastLine*
     * @param firstLine индекс первой строки; включительно
     * @param lastLine индекс последней строки; *исключительно
     */
    private List<String> _logLines(int firstLine, int lastLine) {
        List<String> thisLog = new ArrayList<>();
        try {
            thisLog.addAll(Paths.get(path).toFile().length() == 0
                    ? Collections.singletonList("")
                    : Files.readAllLines(Paths.get(path)).subList(firstLine, lastLine));
        } catch (IOException e) {
            _logAppend("", _stackTrace(e));
        }
        return new ArrayList<>(thisLog);
    }

    /**
     * Возвращает стек-трейс ошибки в виде строки
     */
    static String _stackTrace(Throwable t) {
        StringWriter stack = new StringWriter();
        t.printStackTrace(new PrintWriter(stack));
        return stack.toString();
    }

    /**
     * Добавляет lines в файл _logPath
     */
    void _logAppend(String... lines) {
        try {
            Files.write(Paths.get(path), asList(lines), APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Logg logg = (Logg) o;
        return path.equals(logg.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }
}
