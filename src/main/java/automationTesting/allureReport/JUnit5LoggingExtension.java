package automationTesting.allureReport;

import io.qameta.allure.Allure;
import io.qameta.allure.listener.TestLifecycleListener;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.TestResult;
import org.junit.jupiter.api.extension.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import automationTesting.allureReport.annotations.Version;
import automationTesting.allureReport.parameters.TestParameters;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.platform.commons.util.AnnotationUtils.isAnnotated;
import static automationTesting.allureReport.Attachments.makeScreenShot;
import static automationTesting.allureReport.LoggingService.*;


/**
 * Расширение для JUnit5
 * <p>
 * Требует подключения через @ExtendWith
 * <p>
 * Описание:
 * Добавляет к каждому тесту в Allure
 * файл лога, состоящий из вступления
 * и консольного лога текущего теста
 */
public class JUnit5LoggingExtension implements BeforeAllCallback,
        AfterAllCallback,
        AfterTestExecutionCallback,
        BeforeTestExecutionCallback,
        TestExecutionExceptionHandler,
        TestLifecycleListener {
    private static final Logger _log = LoggerFactory.getLogger(JUnit5LoggingExtension.class);

    /**
     * идентификатор исполняемого теста в Allure
     */
    private static String _currentTestUUID;

    /**
     * для переоткрытия теста
     */
    private static TestResult _testResult;

    @Override
    public void beforeTestExecution(ExtensionContext context) {
        _currentTestUUID = Allure.getLifecycle().getCurrentTestCase().orElse("");
        beforeTest(context);
        _log.info(_stamp("Начало выполнения теста", "\"" + context.getDisplayName() + "\""));
        TestParameters.clear();
        statuses.clear();

        setTMSLink(context);
        setVersion(context);
    }

    private static void setVersion(ExtensionContext context) {
        Method test = context.getRequiredTestMethod();
        if (isAnnotated(test, Version.class)) {
            double version = test.getAnnotation(Version.class).value();
            if (version <= 0)
                fail("Указана некорректная версия ТК `" + test.getName() + "`: " + version);

            String testVersion = "ВЕРСИЯ ТК: " + version;
            Allure.getLifecycle().updateTestCase(executable -> {
                String description = executable.getDescription();
                executable.setDescriptionHtml(isNull(description)
                        ? testVersion
                        : testVersion + "<p>" + description.replace("\n", "<p>"));
            });
        }
    }

    private static void setTMSLink(ExtensionContext context) {
        try {
            String url = ResourceBundle.getBundle("allure").getString("allure.link.tms.pattern");
            String testCaseID = context.getRequiredTestMethod().getName().replaceAll("\\D", "");
            Allure.tms("T" + testCaseID, url.replace("{}", testCaseID));
        } catch (MissingResourceException e) {
            fail("Отсутствует 'allure.properties' в тестовых ресурсах проекта " +
                    "или в 'allure.properties' отсутствует 'allure.link.tms.pattern'!");
        }
    }

    public static Map<String, Status> statuses = new HashMap<>();

    public static void setStatus(Status status) {
        String stepUUID = Allure.getLifecycle().getCurrentTestCaseOrStep().orElse("");
        if (!stepUUID.isEmpty())
            statuses.put(stepUUID, status);
    }

    @Override
    public void afterTestExecution(ExtensionContext context) {
        _log.info(_stamp("Конец выполнения теста", "\"" + context.getDisplayName() + "\""));
        _setParameters();
        afterTest(context);
//        statuses.forEach((stepUUID, status) -> Allure.getLifecycle().updateStep(stepUUID, stepResult ->
//                stepResult.setStatus(status).setStatusDetails(stepResult.getStatusDetails().setKnown(false).setFlaky(true))));
    }

    @Override
    public void afterTestStart(final TestResult result) {
        _testResult = result;
    }

    @Override
    public void afterAll(ExtensionContext context) {
        boolean toAttachEntireLog = Boolean.parseBoolean(System.getProperty("AllureExtension.toAttachEntireLog"));
        if (toAttachEntireLog) {
            _log.info("Переоткрытие теста {} - {}", _currentTestUUID, _testResult);
            //region не работает :(
//            Allure.getLifecycle().scheduleTestCase(_testResult);
//            Allure.getLifecycle().startTestCase(_currentTestUUID);
//            _attachLog(0, "suite.log");
            //endregion
        }
    }

    @Override
    public void beforeAll(ExtensionContext context) {
    }

    @Override
    public void handleTestExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        Allure.getLifecycle().startTestCase(_currentTestUUID);
        makeScreenShot();
        handleException(context, throwable);
        throw throwable;
    }

    /**
     * Заполняет блок параметров теста в Allure
     */
    private static void _setParameters() {
        if (TestParameters.nonEmpty())
            Allure.getLifecycle()
                    .updateTestCase(testResult -> testResult.getParameters().addAll(TestParameters.get()));
    }

    /**
     * Состовляет из сообщений (Начало выполнения теста,
     * "emptyTest1()") штамп вида:
     * <p>
     * ================================================
     * |  Начало выполнения теста  |  "emptyTest1()"  |
     * ================================================
     */
    private static String _stamp(String... messages) {
        String text = String.join("  |  ", messages);
        String line = "\t\t" + IntStream.range(0, text.length() + 6).mapToObj(i -> "=").collect(joining()) + "\n";
        return "\n\n" + line + "\t\t|  " + text + "  |\n" + line;
    }
}