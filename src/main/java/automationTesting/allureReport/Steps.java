package automationTesting.allureReport;

import automationTesting.allureReport.model.AllureStep;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import io.qameta.allure.listener.StepLifecycleListener;
import io.qameta.allure.model.StepResult;

import java.util.Stack;
import java.util.stream.Stream;

import static automationTesting.allureReport.LoggingService.$log;

@SuppressWarnings("unused")
public class Steps /*implements StepLifecycleListener см changelog для ver 1.6 */ {
//    public static final Logger log = LoggerFactory.getLogger(Steps.class);    // заменено на `$log()`

    /**
     * Основной метод (остальные должны ссылаться на него)
     * <p>
     * Реализация 1:
     * Для шага с несколькими ожидаемыми результатами (ОР)
     * <p>
     * Пример использования:
     * <p>
     * step("{name}", () -> {
     * step("expected result #1", () -> {
     * // действия, приводящие к ОР "expected result #1"
     * });
     * <p>
     * step("expected result #2", () -> {
     * // действия, приводящие к ОР "expected result #2"
     * });
     * });
     *
     * @see #step(String, String, Allure.ThrowableRunnableVoid)
     */
    public static void step(String name, Allure.ThrowableRunnableVoid runnable) {
        beforeStep(name);
        try {
            Allure.step(name, runnable);
        } finally {
            afterStep();
        }
    }

    /**
     * Реализация 2:
     * Для шага с единственным ожидаемым результатом.
     * Заменяет конструкцию вида:
     * <p>
     * step("{name}", () -> {
     * result("{expectedResult}", () -> {
     * // step actions
     * });
     * });
     *
     * @see #step(String, Allure.ThrowableRunnableVoid)
     */
    public static void step(String name, String expectedResult, Allure.ThrowableRunnableVoid runnable) {
        step(name, () -> result(expectedResult, runnable));
    }

    /**
     * Реализация 3:
     * Для указания, что достигнут ожидаемый результат из ТК.
     * Заменяет конструкцию вида:
     * <p>
     * step("{prefix}{name}", () -> {});
     * <p>
     * Префикс {prefix} задается системной переменной
     * "AllureExtension.Steps.resultPrefix"
     * (по умолчанию, "[result]: ")
     *
     * @see #step(String, Allure.ThrowableRunnableVoid)
     */                                               // TODO: проверить необходимость runnables
    public static void result(String name, @Deprecated Allure.ThrowableRunnableVoid... runnables) {
        String prefix = System.getProperty("AllureExtension.Steps.resultPrefix", "[result]: ");
        if (runnables.length == 0)
            step(prefix + name, () -> {
            });
        else
            step(prefix + name, runnables[0]);
    }

    /**
     * Добавляет шаг, имя которого получается
     * соединением всех строк {message}{value}
     *
     * @implNote НЕ попадает в историю и в стэк!
     * <p>
     * Предпочтительнее использовать:
     * @see #step(String, Allure.ThrowableRunnableVoid)
     * @see #step(String, String, Allure.ThrowableRunnableVoid)
     */
    @Step("{message}{value}")
    public static String step(String message, String... value) {
        return String.join("", value);
    }


    /**
     * Стек для шагов (позволяет определить вложенность)
     */
    private static final Stack<AllureStep> CURRENT_STEPS = new Stack<>();

    /**
     * Перечень шагов, запущенных в данный момент (последний = текущий)
     */
    public static Stream<AllureStep> currentSteps() {
        return CURRENT_STEPS.stream();
    }

    /**
     * Текущий шаг
     */
    public static AllureStep currentStep() {
        return CURRENT_STEPS.lastElement();
    }

    /**
     * История всех шагов
     */
    private static final Stack<AllureStep> HISTORY = new Stack<>();

    /**
     * Перечень всех завершенных шагов
     */
    public static Stream<AllureStep> history() {
        return HISTORY.stream();
    }

    private static void beforeStep(String name) {
        AllureStep step = new AllureStep(name);
        CURRENT_STEPS.push(step);
        $log().info("\nШаг   `{}`   начинается. {}\n{}\n", name, step.linkToCall(), step);
    }

    private static void afterStep() {
        AllureStep step = CURRENT_STEPS.pop();
        $log().info("\nШаг   `{}`   выполнен. {}\n", step.name, step.linkToCall());
        HISTORY.push(step);
    }

    /**
     * Чтобы листнер работал, необходимо скопировать в доменные ресурсы
     * целевого проекта папку META-INF из ресурсов модуля.
     *
     * Однако, Allure узнает название шага только после выполнения,
     * соответственно, этот вариант НЕ подходит (в течение всего
     * выполнения шага `result.getName()` возвращает "step" и
     * только в afterStepUpdate(..) появляется норм имя).
     *
     * @implNote (резюмирую)
     * Имени нет:
     *  - {@link StepLifecycleListener#beforeStepStart(StepResult)}
     *  - {@link StepLifecycleListener#afterStepStart(StepResult)}
     *  - {@link StepLifecycleListener#beforeStepUpdate(StepResult)}
     * Имя есть:
     *  - {@link StepLifecycleListener#afterStepUpdate(StepResult)}
     *  - {@link StepLifecycleListener#beforeStepStop(StepResult)}
     *  - {@link StepLifecycleListener#afterStepStop(StepResult)}
     *
     * Этим путем можно только добавлять в историю
     */
//    @Override
//    public void beforeStepUpdate(StepResult result) {
//        boolean isNotAddedBefore = ! CURRENT_STEPS.lastElement().name.equals(result.getName());
//        STEPS_VIA_LISTENER.put(result.getName(), isNotAddedBefore);
//        if (isNotAddedBefore)
//            beforeStep(result.getName());
//    }

//    /**
//     * Должен уберечь от двойного добавления и двойного удаления в {@linkplain #CURRENT_STEPS}
//     */
//    private static final Map<String, Boolean> STEPS_VIA_LISTENER = new HashMap<>();

//    @Override
//    public void afterStepStop(StepResult result) {
//        boolean isAddedViaListener = STEPS_VIA_LISTENER.get(result.getName());
//        if (isAddedViaListener)
//            afterStep();
//    }
}

