package automationTesting.allureReport;

import org.junit.jupiter.api.extension.ExtensionContext;
import automationTesting.allureReport.annotations.Log;
import automationTesting.allureReport.annotations.Logs;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static automationTesting.allureReport.LoggingService._log;

public class CommonUtils {

    /**
     * разворачивает список строк в
     * строку со сквозной нумерацией
     */
    static String toString(List<String> l) {
        if (isNull(l))
            return "";
        return IntStream.range(0, l.size())
                .mapToObj(i -> format("\n%5d)  \"%s\"", i, l.get(i)))
                .collect(joining());
    }

    public static void filesWrite(String path, String name, String content, OpenOption... options) {
        try {
            Files.write(Paths.get(path, name), content.getBytes(), options);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Все классы над тестом (в том числе расширяемые)
     */
    public static Set<Class<?>> classesOf(ExtensionContext test) {
        Class<?> c = test.getRequiredTestMethod().getDeclaringClass();
        Set<Class<?>> classes = new HashSet<>();

        classes.add(c);

        while (nonNull(c.getEnclosingClass())) { // добавление вложенных классов
            c = c.getEnclosingClass();
            classes.add(c);
        }

        while (!(c = c.getSuperclass()).equals(Object.class)) // добавление расширяемых классов
            classes.add(c);

        return classes;
    }

    /**
     * Все логи (над тестом, классом теста, классом класса теста и т.д.)
     */
    public static Set<Logg> logsFromAnnotations(ExtensionContext test) {
        List<Annotation> annotations = new ArrayList<>(asList(test.getRequiredTestMethod().getAnnotations()));
        Set<Logg> logs = new HashSet<>();

        classesOf(test).forEach(c -> {
            annotations.addAll(Arrays.asList(c.getAnnotations()));
            for (Annotation a : annotations) {
                if (a instanceof Log)
                    logs.add(new Logg((Log) a));

                if (a instanceof Logs)
                    Arrays.stream(((Logs) a).value())
                            .forEach(l ->  logs.add(new Logg(l)));
            }
            _log.warn("logs = " + logs.stream().map(l -> l.path).collect(toList()));
        });

        return logs;
    }
}
