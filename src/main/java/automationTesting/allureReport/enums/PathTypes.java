package automationTesting.allureReport.enums;

public enum PathTypes {

    /**
     * Каталог локального пользователя
     * @see "System.getProperty("user.home")"
     */
    USER_HOME_BASED,

    /**
     * Каталог проекта
     * @see "System.getProperty("user.dir")"
     */
    USER_DIR_BASED,

    /**
     * Абсолютный путь
     */
    ABS
}
