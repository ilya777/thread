package automationTesting.allureReport;

import io.qameta.allure.model.Parameter;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static io.qameta.allure.util.AspectUtils.getParameters;
import static java.util.stream.Collectors.toList;

@Aspect
class LoggingAspect {
    private static Logger log = LoggerFactory.getLogger(LoggingAspect.class);

    @Pointcut("@annotation(org.junit.jupiter.api.Test) || @annotation(org.junit.jupiter.params.ParameterizedTest)")
    public void withTestAnnotation() {
        //pointcut body, should be empty
    }

    @Pointcut("execution(* *(..))")
    public void anyMethod() {
        //pointcut body, should be empty
    }

    @Before("anyMethod() && withTestAnnotation()")
//    @Before("anyMethod()")
    public void testStart(final JoinPoint joinPoint) {
        final MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        final List<Parameter> parameters = getParameters(methodSignature, joinPoint.getArgs());
        log.info("Method info: " + methodSignature.getName() + CommonUtils.toString(parameters.stream().map(p -> p.getName() + "\"=\"" + p.getValue()).collect(toList())));
    }

    @Around(value = "anyMethod()")
    public Object test(ProceedingJoinPoint pjp) {
        log.info("hi {}", pjp.getSignature());
//        log.info("hi sign1='{}'  sign2='{}'", pjp.getSignature(), joinPoint.getSignature());
        return pjp;
    }
}