package automationTesting.allureReport.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Позволяет зарегистрировать несколько логов
 *
 * Пример:
 *
 * @Logs({
 *     @Log(path = "\roaming\Anketa\logs\log.txt"),
 *     @Log(path = "\build\logs\log.txt", buildBased = true)
 * })
 * class Base {...}
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Logs {
    Log[] value();
}

