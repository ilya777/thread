package automationTesting.allureReport.annotations;

import org.junit.jupiter.api.extension.ExtendWith;
import automationTesting.allureReport.JUnit5LoggingExtension;
import automationTesting.allureReport.enums.PathTypes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Регистрирует лог для прикрепления к отчету allure
 *
 * (!) Мета-аннотация: регистрирует расширение allureExtension для текущего класса,
 *                     поэтому лучше использовать для базового класса
 *
 * buildBased() путь указан относительно
 *                   {true}:  директории сборки [System.getProperty("user.dir")]
 *                   {false}: директории пользователя [System.getProperty("user.home)\build]
 * Примеры:
 * 1)
 * {@literal @Log(path = "\\roaming\\Anketa\\logs\\log.txt") //C:\\users\\admin\\roaming\\Anketa\\logs\\log.txt}
 * class Base {...}
 *
 * 2)
 * @literal @Log(path = "\\build\\logs\\log.txt", buildBased = true) //C:\\users\\admin\\IdeaProjects\\xbrl\\build\\logs\\log.txt
 * class Base {...}
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(JUnit5LoggingExtension.class)
public @interface Log {

    String path() default "";

    PathTypes type() default PathTypes.USER_DIR_BASED;

    /**
     * true = прикреплять только в случае падения
     * false = прикреплять лог в любом случае
     */
    boolean onlyOnFail() default false;

    /**
     * в случае исключения/ошибки добавлять стек
     *
     * в лог ПО не нужно добавлять стек нашей ошибки =)
     */
    boolean appendStacktrace() default false;
}
