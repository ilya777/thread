package automationTesting.allureReport.annotations;

import automationTesting.allureReport.JUnit5LoggingExtension;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Регистрирует версию ТК для прикрепления к отчету allure
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(JUnit5LoggingExtension.class)
public @interface Version {

    double value();
}
