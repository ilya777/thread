package automationTesting.allureReport.annotations;

import org.junit.jupiter.api.extension.ExtendWith;
import automationTesting.allureReport.JUnit5LoggingExtension;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Мета-аннотация
 * Регистрирует данный модуль
 * Заменяет собой @ExtendWith(JUnit5LoggingExtension.class)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(JUnit5LoggingExtension.class)
public @interface ExtendWithAllureExtension {}