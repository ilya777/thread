package automationTesting.allureReport;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;

import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.junit.jupiter.api.Assertions.fail;

public class Attachments {

    // files which should be attached by the root step
    // Map вида <{Имя файла}, {Содержимое файла}>
    private static LinkedHashMap<String, byte[]> rootAttachments = new LinkedHashMap<>();


    public static byte[] makeScreenShot() {
//        return makeScreenShot(now().format(ofPattern("uuuu-MM-dd HH:mm:ss.SSS")));
        return makeScreenShot("Screenshot at " + now().format(ofPattern("HH:mm:ss.SSS")));
    }

    @Attachment(value = "{0}", type = "image/png")
    public static byte[] makeScreenShot(String attachmentName) {
        try {
            Robot robot = new Robot();
            BufferedImage screenshot = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(screenshot, "jpg", baos);
            baos.flush();
            return baos.toByteArray();
        } catch (Exception e) {
            fail("Невозможно сделать screenshot", e);
            return (new byte[]{});
        }
    }

    /**
     * Добавляет в мапу файл, который в
     * последствии должен быть прикреплен к отчету
     *
     * @param fullFileName полное имя файла
     */
    public static void addRootAttachment(String fullFileName, byte[] content) {
        rootAttachments.put(fullFileName, content);
    }

    @Attachment(value = "{0}", type = "text/plain")
    public static byte[] attachException(String filePath) {
        File file = new File(filePath);
        try {
            return Files.readAllBytes(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            e.printStackTrace();
            fail("Невозможно сделать screenshot", e);
            return (new byte[]{});
        }
    }

    /**
     * Прикрепляет файлы к отчету
     */
    @Step("Выгруженные файлы")
    public static void attachFilesIntoRoot() {
        rootAttachments.forEach(Attachments::attachFile);
    }

    private static byte[] attachFile(String fullFileName, byte[] content) {
        String fileExtension = fullFileName.toLowerCase().substring(fullFileName.lastIndexOf('.'));
        return attachFile(fullFileName, fileExtension, content);
    }

    @Attachment(value = "{0}", fileExtension = "{1}")
    public static byte[] attachFile(String fullFileName, String fileExtension, byte[] content) {
        return content;
    }

    @Attachment(value = "{0}", type = "text/plain")
    public static byte[] attachTextFile(String attachmentName, String filePath) {
        File file = new File(filePath);
        try {
            return Files.readAllBytes(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {

            fail("Невозможно сделать screenshot ", e);
            return (new byte[]{});
        }
    }

    @Attachment(value = "{0}")
    public static String attachContent(String attachmentName, String xmlContent) {
        return xmlContent;
    }

    @Attachment(value = "{0}", type = "text/xml")
    public static String attachXmlContent(String attachmentName, String xmlContent) {
        return xmlContent;
    }

    @Attachment(value = "{0}", type = "text/html")
    public static String attachHTMLContent(String attachmentName, String htmlContent) {
        return htmlContent;
    }

    @Attachment(value = "{0}", type = "application/html")
    public static String attachHTMLwebviewContent(String attachmentName, String htmlContent) {
        return htmlContent;
    }

    @Attachment(value = "{0}", type = "text/plain")
    public static String attachTextContent(String attachmentName, String sqlContent) {
        return sqlContent;
    }

    @Attachment(value = "{0}", type = "text/json")
    public static String attachJsonContent(String attachmentName, String sqlContent) {
        return sqlContent;
    }

    public static byte[] attachFile(String filePath, boolean addIntoRoot /* дублировать файл в корневой шаг */) {
        File file = new File(filePath);
        byte[] content;
        try {
            content = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
            if (addIntoRoot)
                addRootAttachment(filePath, content);
            return attachFile(filePath, content);
        } catch (IOException e) {
            fail("Не удалось прикрепить файл \"" + filePath + "\"\n" + e);
        }
        return new byte[0];
    }
}
