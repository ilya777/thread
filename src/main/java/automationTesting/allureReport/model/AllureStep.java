package automationTesting.allureReport.model;

import automationTesting.allureReport.Steps;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

/**
 * Хранимка только для чтения
 *
 * @see Steps#history()
 * @see Steps#currentStep()
 * @see Steps#currentSteps()
 */
public class AllureStep {
    /**
     * 2 сета ниже позволяют управлять условиями для
     * уточнения поиска {@linkplain #placeFrom}
     */
    public static final Set<Predicate<StackTraceElement>> ISNOT_A_PLACE_CONDITIONS;
    /**
     * Если добавить сюда простое имя класса, то при
     * поиске места вызова шага этот класс будет исключен
     */
    public static final Set<String> ISNOT_A_PLACE_CLASSES;
    public final String name;
    public final String number;
    public final StackTraceElement placeFrom;
    public final List<AllureStep> parents;
    public final int depth;

    static {
        ISNOT_A_PLACE_CONDITIONS = Collections.synchronizedSet(new HashSet<>());
        ISNOT_A_PLACE_CLASSES = Collections.synchronizedSet(new HashSet<>());
        ISNOT_A_PLACE_CONDITIONS.add(e ->
                ISNOT_A_PLACE_CLASSES.stream().anyMatch(c -> e.getClassName().endsWith(c)));
        ISNOT_A_PLACE_CLASSES.addAll(Arrays.asList("AllureStep", "Steps"));
    }


    public AllureStep(String name) {
        this.name = name;
        number = parseNumber(name);
        placeFrom = findPlaceFrom();
        parents = Collections.unmodifiableList(Steps.currentSteps().collect(toList()));
        depth = parents.size();
        start = Instant.now();
    }

    private static StackTraceElement findPlaceFrom() {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        AtomicInteger i = new AtomicInteger(4);
        while (ISNOT_A_PLACE_CONDITIONS.stream().anyMatch(cond -> cond.test(trace[i.get()])))
            if (i.getAndAdd(1) > 20)
                return trace[4];
        return trace[i.get()];
    }

    public static String parseNumber(String name) {
        name = name.replaceAll("[\\s\\u00A0]+", ""); // убираю все пробелы
        boolean hasNumber = name.toLowerCase().matches("\\[steps?[\\d-]+]:.*"); // `[step1]:имя` или `[steps1-3]:`
        if (hasNumber) {
            Pattern p = Pattern.compile("[\\d-]+");
            Matcher m = p.matcher(name);
            if (m.find()) // только первое совпадение
                return m.group();
        }
        return "";
    }

    private final Instant start;

    public Duration duration() {
        return Duration.between(start, Instant.now());
    }

    /**
     * Ссылка на строку, из которой вызван шаг. Идея подхватывает эту ссылку как гиперссылку
     */
    public String linkToCall() {
        return "(" + placeFrom.getFileName() + ":" + placeFrom.getLineNumber() + ")"; // без скобок Идея не распознает
    }

    /**
     * Представляет данный шаг в виде строки:
     * StepsTests::currentTests(): #2 { #2-5 { StepsTests::otherMethod(): №1 { #1-10 { ->SimpleTest1::t2(): [Steps 1 - 11]: T2 - Простой т...<- } } } }
     * <p>
     * или, если разнести строки:
     * StepsTests::currentTests(): #2 {                                            // глубина=0; прапрадед;  [Step 2]: ...
     * #2-5 {                                                                  // глубина=1; прадед;  [Steps 2 - 5]: ...
     * StepsTests::otherMethod(): №1 {                                     // глубина=2; дед; нет своего номера в {name}, поэтому указан порядковый №
     * #1-10 {                                                         // глубина=3; непосредственный родитель; [Steps 1 - 10]: ...
     * ->SimpleTest1::t2(): [Steps 1 - 11]: T2 - Простой т...<-    // глубина=4; this (ТЕКУЩИЙ тест)
     * }
     * }
     * }
     * }
     * <p>
     * Этот пример показывает ситуацию:
     * 1) в классе `StepsTests` из метода `currentTests()` запущен "корневой" шаг, с номером 2.
     * 2) из корневого шага (в том же методе) запущен шаг {@code step("[Steps 2 - 5]: ...", () -> ...)}
     * 3) в шаге "[Steps 2 - 5]" вызывается метод `otherMethod()` из того же класса. В вызванном методе запущен шаг без
     * номера в имени -> у шага отображается порядковый номер ("№")
     * 4) далее запущен шаг {@code step("[Steps 1 - 10]: ...", () -> ...)}
     * 5) и уже из "[Steps 1 - 10]" вызывается  {@code new SimpleTest1().t2()}, внутри которого запустился текужий шаг.
     * Закрываться шаги будут в обратной последовательности.
     *
     * @implNote 1) отображение имени класса и метода
     * - не дублируется. В примере выше шаг с глубиной (1) находится в том же методе, что и (0). А (2) - уже в другом
     * <p>
     * 2) отображение порядкового номера
     * - "#": Если порядковый номер указан в {name} (например, {@code step("[Steps 2 - 5]: ...", () -> ...)})
     * - "№": Если номер отсутствует (например, {@code step("Открытие формы ля-ля", () -> ...)})
     * <p>
     * 3) фигурные скобки указывают на вложенность
     * <p>
     * 4) по умолчанию имя шага обрезается до 33 символов (см {@linkplain #toString(int)})
     * <p>
     * 5) текущий тест окружен стрЕлками ("->" и "<-")
     * @see #number()
     * @see #toShortString()
     */
    @Override
    public String toString() {
        return toString(33);
    }

    public String toString(int maxNameLength) {
        String generationDelimiter = " { ";
        String itsName = name.length() > maxNameLength
                ? name.substring(0, maxNameLength - 3) + "..." // maxNameLength не должно быть короче 3 символов :)
                : name;
        String itsMethodName = methodName().isEmpty() ? "" : methodName() + ": ";
        Stream<String> itsStream = Stream.of("->" + itsMethodName + itsName + "<-");
        Stream<String> parentsSt = parents.stream().map(AllureStep::toShortString);
        String closingBrackets = IntStream.range(0, parents.size()).mapToObj(i -> " }").collect(joining());
        return Stream.concat(parentsSt, itsStream).collect(joining(generationDelimiter)) + closingBrackets;
    }

    private String methodName() {
        if (placeFrom.getMethodName().contains("$"))
            return "";
        String className = placeFrom.getClassName(); // каноничное имя
        if (className.contains(".")) // Class.forName(...) надо оборачивать в try/catch... :\
            className = className.substring(className.lastIndexOf(".") + 1); // после последней точки
        return className + "::" + placeFrom.getMethodName() + "()";
    }

    /**
     * Порядковый номер
     * - "#": Если порядковый номер указан в {name} (например, {@code step("[Steps 2 - 5]: ...", () -> ...)})
     * - "№": Если номер отсутствует (например, {@code step("Открытие формы ля-ля", () -> ...)})
     */
    public String number() {
        return number.isEmpty() ? "№" + numInHistory() : "#" + number;
    }

    /**
     * Код:
     * class Scenario_T000 {
     * <p>
     * aTest
     * void t0() {
     * ...
     * step("[Step 3]: T123 - Симпл-тест", () -> {                     // step1
     * step("[Step 1]: Первый шаг вложенного симпла", () -> {      // step2
     * ...
     * });
     * });
     * }
     * }
     * <p>
     * Результат:
     * step1.toShortString()   // Scenario_T000::t0(): #3
     * step2.toShortString()   // #1
     * step1.toString()        // ->Scenario_T000::t0(): [Step 3]: T123 - Симпл-тест<-
     * step2.toString()        // Scenario_T000::t0(): #3 { ->[Step 1]: Первый шаг вложенног...<- }
     */
    public String toShortString() {
        String methodName = methodName().isEmpty() ? "" : methodName() + ": ";
        return methodName + number();
    }

    /**
     * Вычисляет порядковый номер теста по истории (если номер не указан в {@linkplain #name})
     */
    private int numInHistory() {
        int n = 1;
        LinkedList<AllureStep> history = Steps.history().collect(toCollection(LinkedList::new));
        while (!history.isEmpty()) {
            AllureStep current = history.removeLast();
            if (current.depth < depth)
                break;
            if (current.depth == depth)
                n++;
        }
        return n;
    }
}
