package automationTesting.allureReport.parameters;

/**
 * Для организации ввода нескольких
 * параметров "цепочкой" (pipeline)
 *
 * Для задания параметров теста,
 * @see StepParameters
 */
public class StepParameter {

    StepParameter() { /* сокрытие конструктора */}

    public StepParameter add(String name, Object value) {
        StepParameters.add(name, value.toString());
        return this;
    }
}