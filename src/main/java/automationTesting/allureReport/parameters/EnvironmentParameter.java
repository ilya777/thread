package automationTesting.allureReport.parameters;

/**
 * Для организации ввода нескольких
 * параметров "цепочкой" (pipeline)
 *
 * Для задания параметров теста,
 * @see EnvironmentParameters
 */
public class EnvironmentParameter {

    EnvironmentParameter() { /* сокрытие конструктора */}

    public EnvironmentParameter add(String name, Object value) {
        return EnvironmentParameters.add(name, value.toString());
    }
}