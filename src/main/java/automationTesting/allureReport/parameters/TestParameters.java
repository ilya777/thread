package automationTesting.allureReport.parameters;

import io.qameta.allure.model.Parameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;


/**
 * Наполнение, использование и очистка
 * параметров теста.
 *
 * Тестовые параметры - специальный блок
 * в тесте; служит для отражения входных
 * данных и констант.
 *
 * Пример использования:
 *
 *  TestParameters
 *      .add("Имя файла", "C:\\456.xml")
 *      .add("Наименование оси", "Ось");
 */
public class TestParameters {
    static final Map<String, String> testParameters = new HashMap<>();


    public static TestParameter add(String name, Object value) {
        testParameters.put(name, value.toString());
        return new TestParameter();
    }

    /**
     * Позволяет заменить значение параметра на указанное, если исходное равно null
     * @param name - наименование параметра
     * @param value - значение параметра
     * @param nullReplacement - запасное значение
     */
    public static TestParameter add(String name, Object value, String nullReplacement) {
        return nonNull(value) ? add(name, value) : add(name, nullReplacement);
    }

    /**
     * Добавляет параметр к отчету только в том случае, когда его значение != null
     */
    public static TestParameter addNonNull(String name, Object value) {
        return nonNull(value) ? add(name, value) : new TestParameter();
    }

    public static void clear() {
        testParameters.clear();
    }

    /**
     * возвращает TestParameters в виде списка
     * объектов {@linkplain Parameter}
     */
    public static List<Parameter> get() {
        List<Parameter> parameters = new ArrayList<>(testParameters.size());
        testParameters.forEach((n, v) -> parameters.add(new Parameter().setName(n).setValue(v)));
        return parameters;
    }

    public static boolean nonEmpty() {
        return !testParameters.isEmpty();
    }
}
