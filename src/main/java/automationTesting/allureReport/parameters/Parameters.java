package automationTesting.allureReport.parameters;

public interface Parameters<T> {

    T add(Object name, Object value);

    T add(String name, Object value, String nullReplacement);

    T addNonNull(Object name, Object value);

}
