package automationTesting.allureReport.parameters;

import static automationTesting.allureReport.parameters.TestParameters.testParameters;


/**
 * Для организации ввода нескольких
 * параметров "цепочкой" (pipeline)
 *
 * Для задания параметров теста,
 * @see TestParameters
 */
public class TestParameter {

    TestParameter() { /* сокрытие конструктора */}

    public TestParameter add(String name, Object value) {
        testParameters.put(name, value.toString());
        return this;
    }
}