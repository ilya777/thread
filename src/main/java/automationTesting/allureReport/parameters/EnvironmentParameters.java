package automationTesting.allureReport.parameters;

import io.qameta.allure.model.Parameter;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static java.nio.file.StandardOpenOption.*;
import static automationTesting.allureReport.CommonUtils.filesWrite;

/**
 * Предоставляет API для добавления
 * описания окружения на главную
 * страницу отчета
 *
 * Наполнение, использование и очистка
 * параметров теста.
 *
 * Пример использования:
 *
 *  EnvironmentParameters
 *      .add("Версия приложения", "1.765.345")
 *      .add("Версия БД", "1.563");
 */
@SuppressWarnings("unused")
public class EnvironmentParameters {
    static final Map<String, String> environmentParameters = new HashMap<>();


    public static EnvironmentParameter add(String name, Object value) {
        environmentParameters.put(name, value.toString());
        Carrier.addToFile(parameter(name, value));
        return new EnvironmentParameter();
    }

    public static void clear() {
        environmentParameters.clear();
    }

    public static List<Parameter> get() {
        List<Parameter> envParameters = new ArrayList<>();
        environmentParameters.forEach((k, v) -> envParameters.add(new Parameter().setName(k).setValue(v)));
        return envParameters;
    }


    private static String parameter(String k, Object v) {
        return "<parameter>\n" +
               "\t    <key>" + k + "</key>\n" +
               "\t    <value>" + v + "</value>\n" +
               "\t</parameter>\n";
    }

    /**
     * отвечает за создание файла environment.xml в allure-results
     */
    private static class Carrier {
        static String content = "\n" +
                "<environment>\n" +
                "    <!--{param}-->\n" +
                "</environment>\n";

        static void addToFile(String parameter) {
            _insertInContent(parameter);

            filesWrite(
                    resultsDir(), "environment.xml",
                    content,
                    CREATE, WRITE, TRUNCATE_EXISTING
            );
        }

        private static void _insertInContent(String param) {
            String anchor = "<!--{param}-->";
            content = content.replace(anchor, param + "\n\t" + anchor);
        }

        private static String resultsDir() {
            Path defDestinationPath = Paths.get(System.getProperty("user.dir"), "build", "allure-results");
            return System.getProperty("allure.results.directory", defDestinationPath.toString());
        }
    }

}
