package automationTesting.allureReport.parameters;

import io.qameta.allure.Allure;
import io.qameta.allure.model.Parameter;

import static java.util.Objects.nonNull;


/**
 * Предоставляет метод для добавления
 * параметров в шаг теста
 *
 * Пример использования:
 *
 *  StepParameters
 *      .add("Имя файла", "C:\\456.xml")
 *      .add("Наименование оси", "Ось");
 */
public class StepParameters {

    public static StepParameter add(String name, Object value) {
        if (isTest())
            TestParameters.add(name, value);
        else
            Allure.getLifecycle().getCurrentTestCaseOrStep().ifPresent(stepOrTest ->
                    Allure.getLifecycle().updateStep(stepOrTest, stepResult ->
                            stepResult.getParameters().add(new Parameter().setName(name).setValue(value.toString()))));

        return new StepParameter();
    }

    /**
     * Позволяет заменить значение параметра на указанное, если исходное равно null
     * @param name - наименование параметра
     * @param value - значение параметра
     * @param nullReplacement - запасное значение
     */
    public static StepParameter add(String name, Object value, String nullReplacement) {
        return nonNull(value) ? add(name, value) : add(name, nullReplacement);
    }

    /**
     * Добавляет параметр к отчету только в том случае, когда его значение != null
     */
    public static StepParameter addNonNull(String name, Object value) {
        return nonNull(value) ? add(name, value) : new StepParameter();
    }

    private static boolean isTest() {
        String testUUID = Allure.getLifecycle().getCurrentTestCase().orElse("");
        String stepOrTest = Allure.getLifecycle().getCurrentTestCaseOrStep().orElse("");
        return stepOrTest.equals(testUUID);
    }
}
